﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Provider.Gismeteo.Logic;


namespace Provider.Gismeteo.Testing
{
    class Program
    {
        static void Main(string[] args)
        {
           // Test();


            System.Threading.Thread.Sleep(500000000);
        }

        static async void Test()
        {
            //CultureInfo info = new CultureInfo()

            //CultureInfo.GetCultureInfoByIetfLanguageTag();

            AccessLink accessLink = new AccessLink();

            IReadOnlyList<ITown> towns = await accessLink.GetTowns();

            List<IWeather> weathers = new List<IWeather>();

            foreach (ITown town in towns)
            {
                IReadOnlyList<IWeather> range = await accessLink.GetWeathers(town.Link);

                weathers.AddRange(range);
            }

            //IReadOnlyList<IWeather> weathers = await accessLink.GetWeathers("https://www.gismeteo.ru/weather-slutsk-4266/");
        }
    }
}
