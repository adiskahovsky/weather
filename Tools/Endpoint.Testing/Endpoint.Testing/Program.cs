﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Endpoint.Testing
{

    [DataContract]
    public class WeatherData
    {
        #region Constructors
        public WeatherData(float temperature, float wind, string preciptitation, string state, TimeSpan time)
        {
            this.Temperature = temperature;
            this.Wind = wind;
            this.Precipitation = preciptitation;
            this.State = state;
            this.Time = time;
        }
        #endregion

        #region Properties
        [DataMember]
        public float Temperature { get; }

        [DataMember]
        public float Wind { get; }

        [DataMember]
        public string Precipitation { get; }

        [DataMember]
        public string State { get; }

        [DataMember]
        public TimeSpan Time { get; }
        #endregion
    }

    [DataContract]
    public class WeatherOverview
    {
        public WeatherOverview(WeatherData[] data)
        {
            this.Data = data;
        }

        [DataMember]
        public IReadOnlyList<WeatherData> Data { get; }
    }

    public interface ITown
    {
        Guid Id { get; }
        string Title { get; }
    }

    [DataContract]
    public class Result<T>
    {
        public Result()
        {
        }

        [DataMember]
        public T[] Data        { get; set; }
    }

        [DataContract]
    public class TownModel 
    {
        #region Constructors
        public TownModel(Guid id, string title)
        {
            this.Id = id;
            this.Title = title;
        }
        public TownModel()
        {
        }
        #endregion

        #region Fields
        [DataMember]
        private Guid id;

        [DataMember]
        private string title;
        #endregion

        #region Properties
        [DataMember]
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }
        #endregion
    }

    [ServiceContract]
    public interface IWeatherService
    {
        [OperationContract]
        Task<Result<TownModel>> GetTowns();

        [OperationContract]
        Task<Result<WeatherOverview>> GetOverview(Guid towId, DateTime date);

        [OperationContract]
        TownModel Test();
    }

    class Program
    {
        static void Main(string[] args)
        {
            Execute().Wait();
        }

        static async Task Execute()
        {
            EndpointAddress endpoint = new EndpointAddress("http://localhost:64096/WeatherService.svc");
            BasicHttpBinding binding = new BasicHttpBinding();

            ChannelFactory<IWeatherService> channel = new ChannelFactory<IWeatherService>(binding, endpoint);

            IWeatherService service = channel.CreateChannel();

            Result<TownModel> test = await service.GetTowns();
        }
    }
}
