﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AngleSharp.Dom;


namespace Provider.Gismeteo.Logic
{
    internal class Weather : IWeather
    {
        #region Constructors
        public Weather(IElement temperature, IElement wind, IElement preciptitation, IElement state, IElement date, IElement time)
        {
            this.Setup(temperature, wind, preciptitation, state, date, time);
        }
        public Weather(IElement temperature, IElement wind, IElement state, IElement date, IElement time)
        {
            this.Setup(temperature, wind, state, date, time);
        }
        #endregion

        #region IWeather
        float IWeather.Temperature
        {
            get { return this.Temperature; }
        }
        float IWeather.Wind
        {
            get { return this.Wind; }
        }
        string IWeather.Precipitation
        {
            get { return this.Precipitation; }
        }
        string IWeather.State
        {
            get { return this.State; }
        }
        DateTime IWeather.Point
        {
            get { return this.Point; }
        }
        #endregion

        #region Properties
        private float Temperature { get; set; }
        private float Wind { get; set; }
        private string Precipitation { get; set; }
        private string State { get; set; }
        private DateTime Point { get; set; }
        #endregion

        #region Assistants
        private void Setup(IElement temperature, IElement wind, IElement preciptitation, IElement state, IElement date, IElement time)
        {
            this.SetupTemperature(temperature);
            this.SetupWind(wind);
            this.SetupPrecipitation(preciptitation);
            this.SetupState(state);
            this.SetupPoint(date, time);
        }
        private void Setup(IElement temperature, IElement wind, IElement state, IElement date, IElement time)
        {
            this.SetupTemperature(temperature);
            this.SetupWind(wind);
            this.SetupState(state);
            this.SetupPoint(date, time);
        }

        private void SetupTemperature(IElement element)
        {
            int? temperature = this.ParseTemperature(element);
            if (temperature == null)
                throw new Exception(nameof(temperature));

            this.Temperature = temperature.Value;
        }
        private void SetupWind(IElement element)
        {
            (float From, float? Till)? wind = this.ParseWind(element);
            if(wind == null)
                throw new Exception(nameof(wind));

            if (wind?.Till == null)
            {
                this.Wind = wind.Value.From;
            }
            else
            {
                float till = wind.Value.Till.Value;
                float from = wind.Value.From;

                this.Wind = (from + till) / 2;
            }
        }
        private void SetupPrecipitation(IElement element)
        {
            string precipitation = this.ParsePrecipitation(element);
            if (precipitation == null)
                throw new Exception(nameof(precipitation));

            this.Precipitation = precipitation;
        }
        private void SetupState(IElement element)
        {
            string state = this.ParseState(element);
            if (state == null)
                throw new Exception(nameof(state));

            this.State = state;
        }
        private void SetupPoint(IElement date, IElement time)
        {
            DateTime? point = this.ParsePoint(date, time);
            if (point == null)
                throw new Exception(nameof(point));

            this.Point = point.Value;
        }

        private int? ParseTemperature(IElement element)
        {
            IElement[] childs = element.Children.ToArray();

            foreach (IElement child in childs)
            {
                if (this.CheckTemperature(child) == true)
                {
                    NumberFormatInfo format = new NumberFormatInfo();
                    format.NegativeSign = "−";

                    string content = child.Text().Trim();
                    if (int.TryParse(content, NumberStyles.Integer, format, out int temperature) == true)
                        return temperature;
                }
            }

            return null;
        }
        private (float From, float? Till)? ParseWind(IElement element)
        {
            IElement[] childs = element.Children.ToArray();

            foreach (IElement child in childs)
            {
                if (this.CheckWind(child) == true)
                {
                    string content = child.Text().Trim();
                    if (float.TryParse(content, out float wind) == false)
                    {
                        string[] period = content.Split('-');
                        if (period.Length != 2)
                            return null;

                        if (float.TryParse(period.First(), out float from) == false)
                            return null;

                        if (float.TryParse(period.Last(), out float till) == false)
                            return null;

                        return (from, till);
                    }

                    return (wind, null);
                }
            }

            return null;
        }
        private string ParsePrecipitation(IElement element)
        {
            IElement[] childs = element.Children.ToArray();

            foreach (IElement child in childs)
            {
                if (this.CheckPrecipitation(child) == true)
                    return  child.Text().Trim();
            }

            return null;
        }
        private string ParseState(IElement element)
        {
            IElement[] childs = element.Children.ToArray();

            foreach (IElement child in childs)
            {
                if (this.CheckState(child) == true)
                    return child.Attributes["data-text"].Value.Trim();
            }

            return null;
        }
        private DateTime? ParsePoint(IElement elementDate, IElement elementTime)
        {
            // Date
            DateTime? date = this.ParseDate(elementDate);

            // Time
            TimeSpan? time = this.ParseTime(elementTime);

            if (date == null || time == null)
                return null;

            return date + time;
        }
        private DateTime? ParseDate(IElement element)
        {
            IElement child = element.Children.FirstOrDefault();
            if (this.CheckDate(child) == false)
                return null;

            string text = child.Text().Trim();
            if (text == "Сегодня")
                return DateTime.Now.Date;

            if (text == "Завтра")
                return DateTime.Now.AddDays(1).Date;

            return null;
        }
        private TimeSpan? ParseTime(IElement element)
        {
            // Childs
            IElement[] childs = element.Children.ToArray();
            if (this.CheckTime(childs) == false)
                return null;

            // Time
            string value = childs.First().Text().Trim();
            if (int.TryParse(value, out int hours) == false)
                return null;

            value = childs.Last().Text().Trim();
            if (int.TryParse(value, out int minutes) == false)
                return null;

            TimeSpan time = new TimeSpan(hours, minutes, 0);

            return time;
        }


        private DateTime? ExtractDate(string text)
        {
            string[] separator = this.GetSeparator();
            string[] formats = this.GetFormats();

            string[] entries = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            if (entries.Length == 3)
            {
                string entry = entries[1];
                if (DateTime.TryParseExact(entry, formats, null, DateTimeStyles.None, out DateTime point))
                    return point;

                return null;
            }
            else
            {
                foreach (string entry in entries)
                {
                    if (DateTime.TryParseExact(entry, formats, null, DateTimeStyles.None, out DateTime point))
                        return point.Date;
                }
            }

            return null;
        }
        private string[] GetFormats()
        {
            List<string> formats = new List<string>(2);
            formats.Add("yyyy-MM-dd HH:mm:ss");
            formats.Add("yyyy-MM-dd HH:mm:ss(UTC)");

            return formats.ToArray();
        }
        private string[] GetSeparator()
        {
            List<string> separator = new List<string>(2);
            //separator.Add(",UTC:");
            separator.Add(": ");
            separator.Add(",");

            return separator.ToArray();
        }

        private bool CheckTemperature(IElement element)
        {
            if (element == null)
                return false;

            if (element.TagName != "SPAN")
                return false;

            ITokenList classes = element.ClassList;

            if (classes.Contains("unit") == false)
                return false;

            if (classes.Contains("unit_temperature_c") == false)
                return false;

            return true;
        }
        private bool CheckWind(IElement element)
        {
            if (element == null)
                return false;

            if (element.TagName != "SPAN")
                return false;

            ITokenList classes = element.ClassList;

            if (element.ClassList.Contains("unit") == false)
                return false;

            if (element.ClassList.Contains("unit_wind_m_s") == false)
                return false;

            return true;
        }
        private bool CheckPrecipitation(IElement element)
        {
            if (element == null)
                return false;

            if (element.TagName != "DIV")
                return false;

            ITokenList classes = element.ClassList;

            if (element.ClassList.Contains("w_prec__value") == false)
                return false;

            return true;
        }
        private bool CheckState(IElement element)
        {
            if (element == null)
                return false;

            if (element.TagName != "SPAN")
                return false;

            ITokenList classes = element.ClassList;
            if (element.ClassList.Contains("tooltip") == false)
                return false;

            INamedNodeMap attributes = element.Attributes;
            if (attributes.Any(item => item.Name == "data-text") == false)
                return false;

            return true;
        }
        private bool CheckDate(IElement element)
        {
            if (element == null)
                return false;

            if (element.TagName != "A" && element.TagName != "SPAN")
                return false;

            ITokenList classes = element.ClassList;

            if (classes.Contains("white") == false)
                return false;

            if (classes.Contains("subnav_link") == false)
                return false;

            return true;
        }
        private bool CheckTime(IElement[] elements)
        {
            if (elements == null)
                return false;

            if (elements.Length != 2)
                return false;

            foreach (IElement element in elements)
            {
                if (element.TagName != "SPAN" && element.TagName != "SUP")
                    return false;
            }

            return true;
        }
        #endregion

    }
}
