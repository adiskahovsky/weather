﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;


namespace Provider.Gismeteo.Logic
{
    internal class Parser
    {
        #region Enums
        public enum By
        {
            ClassName
        }
        #endregion

        #region Constructors
        public Parser(string source)
        {
            this.Setup(source);
        }
        #endregion

        #region Properties
        private IHtmlDocument Document { get; set; }
        #endregion

        #region Methods
        public IReadOnlyList<IElement> GetElements(string value, By by = By.ClassName)
        {
            IHtmlCollection<IElement> elements = this.Document.GetElementsByClassName(value);

            return elements.ToArray();
        }
        #endregion

        #region Assistants
        private void Setup(string source)
        {
            HtmlParser parser = new HtmlParser();

            IHtmlDocument document = parser.ParseDocument(source);

            this.Document = document;
        }
        #endregion
    }
}
