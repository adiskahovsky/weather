﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;

using Provider.Gismeteo.Settings;


namespace Provider.Gismeteo.Logic
{
    internal class Towns : Service
    {
        #region Constructors
        #endregion

        #region Methods
        public async Task<IReadOnlyList<ITown>> Retrieve()
        {
            Configuration configuration = new Configuration();

            string link = configuration.Link;

            string content = await this.MakeRequest(link);

            IReadOnlyList<ITown> towns = this.GetTowns(content);

            return towns;
        }
        #endregion

        #region Assistants
        private IReadOnlyList<ITown> GetTowns(string content)
        {
            HtmlParser parser = new HtmlParser();

            IHtmlDocument document = parser.ParseDocument(content);

            IElement[] elements = document.GetElementsByClassName("cities_item").ToArray();

            List<ITown> towns = new List<ITown>(elements.Length);

            foreach (IElement element in elements)
            {
                ITown town = new Town(element);

                towns.Add(town);
            }

            return towns;
        }
        #endregion
    }
}
