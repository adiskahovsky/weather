﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Provider.Gismeteo.Logic
{
    public interface ITown
    {
        string Title { get; }
        string Link { get; }
    }
}
