﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;

using Provider.Gismeteo.Settings;


namespace Provider.Gismeteo.Logic
{
    internal class Weathers : Service
    {
        #region Classes
        private static class Selector
        {
            public static string Dates = ".subnav_nav > li";
            public static string Container = "div.widget__container:nth-child(1)";
            public static string Times = ".widget__row_time > .widget__item > .w_time";
            public static string States = ".widget__row_icon > div > div";
            public static string Temperatures = ".widget__row_temperature > div > div > div > div";
            public static string Winds = ".widget__row_wind-or-gust > .widget__item > div > div";
            public static string Precipitations = ".widget__row_precipitation > .widget__item > div";
        }
        #endregion

        #region Methods
        public async Task<IReadOnlyList<IWeather>> Retrieve(string link)
        {
            Configuration configuration = new Configuration();

            string content = await this.MakeRequest(link);

            IReadOnlyList<IWeather> weathers = this.GetWeathers(content);

            return weathers;
        }
        #endregion

        #region Assistants
        private IReadOnlyList<IWeather> GetWeathers(string content)
        {
            int rows = 5; // TODO: In config.

            HtmlParser parser = new HtmlParser();

            IHtmlDocument document = parser.ParseDocument(content);

            IElement[] dates = document.QuerySelectorAll(Selector.Dates).ToArray();

            IElement date = this.GetActiveDate(dates);
            if (date == null)
                throw new Exception("Cannot find active date.");

            IElement container = document.QuerySelector(Selector.Container);

            IElement[] times = container.QuerySelectorAll(Selector.Times).ToArray();
            IElement[] states = container.QuerySelectorAll(Selector.States).ToArray();
            IElement[] temperatures = container.QuerySelectorAll(Selector.Temperatures).ToArray();
            IElement[] winds = container.QuerySelectorAll(Selector.Winds).ToArray();
            IElement[] precipitations = container.QuerySelectorAll(Selector.Precipitations).ToArray();

            int total;

            if (precipitations.Any() == false)
            {
                precipitations = null;

                rows--;

                total = times.Length + states.Length + temperatures.Length + winds.Length;
            }
            else
            {
                total = times.Length + states.Length + temperatures.Length + winds.Length + precipitations.Length;
            }

            int count = total / rows;
            if (count != times.Length) 
                throw new Exception("The number of columns in all rows must match.");

            List<IWeather> weathers = new List<IWeather>(count);

            for (int index = 0; index < count; index++)
            {
                IWeather weather = null;
                if(precipitations != null)
                    weather = new Weather(temperatures[index], winds[index], precipitations[index], states[index], date, times[index]);
                else
                    weather = new Weather(temperatures[index], winds[index], states[index], date, times[index]);

                weathers.Add(weather);
            }

            return weathers;
        }
        #endregion

        #region Assistants
        private IElement GetActiveDate(IElement[] dates)
        {
            foreach (IElement date in dates)
            {
                if (date.ClassList.Contains("active") == true)
                    return date;
            }

            return null;
        }
        #endregion
    }
}
