﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using AngleSharp.Dom;

using Provider.Gismeteo.Settings;


namespace Provider.Gismeteo.Logic
{ 
    internal class Town : ITown
    {
        #region Constructors
        public Town(IElement element)
        {
            this.Setup(element);
        }
        #endregion

        #region ITown
        string ITown.Title
        {
            get { return this.Title; }
        }
        string ITown.Link
        {
            get { return this.Link; }
        }
        #endregion

        #region Properties
        private string Title { get; set; }
        private string Link { get; set; }
        #endregion

        #region Assistants
        private void Setup(IElement element)
        {
            if (this.CheckRoot(element) == false)
                throw new Exception(nameof(element));

            (string Title, string Href)? data = this.Parse(element);
            if (data == null)
                throw new Exception(nameof(data));

            Configuration configuration = new Configuration();

            string link = $"{configuration.Link}/{data?.Href}";

            this.Title = data?.Title;
            this.Link = link;
        }

        private (string Title, string Href)? Parse(IElement element)
        {
            IElement anchor = this.GetAnchor(element.Children.ToArray());
            IElement span = this.GetSpan(anchor.Children.ToArray());

            if (anchor == null || span == null)
                return null;

            string title = span.Text().Trim();
            string href = anchor.Attributes["href"].Value;

            if (title == null || href == null)
                return null;

            return (title, href);
        }
        private IElement GetAnchor(IElement[] elements)
        {
            foreach (IElement element in elements)
            {
                if (this.CheckAnchor(element) == true)
                    return element;
            }

            return null;
        }
        private IElement GetSpan(IElement[] elements)
        {
            foreach (IElement element in elements)
            {
                if (this.CheckSpan(element) == true)
                    return element;

            }

            return null;
        }

        private bool CheckRoot(IElement root)
        {
            if (root == null)
                return false;

            if (root.TagName != "DIV")
                return false;

            ITokenList classes = root.ClassList;

            if (classes.Contains("cities_item") == false)
                return false;

            return true;
        }
        private bool CheckSpan(IElement span)
        {
            if (span == null)
                return false;

            if (span.TagName != "SPAN")
                return false;

            ITokenList classes = span.ClassList;

            if (classes.Contains("cities_name") == false)
                return false;

            return true;
        }
        private bool CheckAnchor(IElement anchor)
        {
            if (anchor == null)
                return false;

            if (anchor.TagName != "A")
                return false;

            ITokenList classes = anchor.ClassList;

            if (classes.Contains("link") == false)
                return false;

            if (classes.Contains("no_border") == false)
                return false;

            if (classes.Contains("blue") == false)
                return false;

            if (classes.Contains("cities_link") == false)
                return false;

            return true;
        }
        #endregion
    }
}
