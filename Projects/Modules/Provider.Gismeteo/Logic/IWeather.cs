﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Provider.Gismeteo.Logic
{
    public interface IWeather
    {
        float Temperature { get; }
        float Wind { get; }
        string Precipitation { get; }
        string State { get; }
        DateTime Point { get; }
    }
}
