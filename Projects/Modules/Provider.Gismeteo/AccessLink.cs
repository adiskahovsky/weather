﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Provider.Gismeteo.Logic;


namespace Provider.Gismeteo
{
    public interface IAccessLink
    {

    }

    public class AccessLink : IAccessLink
    {
        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region IAccessLink
        #endregion

        public async Task<IReadOnlyList<ITown>> GetTowns()
        {
            Towns towns = new Towns();

            return await towns.Retrieve();
        }

        public async Task<IReadOnlyList<IWeather>> GetWeathers(string link)
        {
            Weathers weathers = new Weathers();

            return await weathers.Retrieve(link);
        }
    }
}
