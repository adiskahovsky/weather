﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;

using Newtonsoft.Json.Linq;


namespace Provider.Gismeteo.Settings
{
    public class Configuration
    {
        #region Constructors
        public Configuration()
        {
            this.Setup();
        }
        #endregion

        #region Properties
        public string Countries { get; private set; }
        public string Link { get; private set; }
        #endregion

        #region Assistants
        private void Setup()
        {
            // Directory
            string directory = this.GetSolutionDirectory();

            // Assembly
            Assembly assembly = Assembly.GetExecutingAssembly();

            // Name
            string name = assembly.ManifestModule.Name.Replace(".dll", ".json");

            // File
            string file = Directory.GetFiles(directory, name, SearchOption.AllDirectories).First();

            // Content
            string content = File.ReadAllText(file);

            // Json
            JObject json = JObject.Parse(content); 

            // Setup
            this.SetupCountries(json);
            this.SetupLink(json);
        }
        private void SetupCountries(JObject json)
        {
            string countries = json.GetValue(nameof(this.Countries)).Value<string>();

            this.Countries = countries;
        }
        private void SetupLink(JObject json)
        {
            string link = json.GetValue(nameof(this.Link)).Value<string>();

            this.Link = link;
        }

        private string GetSolutionDirectory()
        {
            string current = Directory.GetCurrentDirectory();

            DirectoryInfo parent = Directory.GetParent(current);

            string path = parent.Parent.Parent.Parent.Parent.FullName;

            return path;
        }
        #endregion
    }
}
