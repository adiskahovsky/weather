﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace Weather.Api.Location
{
    public class Town
    {
        #region Constructors
        public Town(Guid id, string title)
        {
            this.Id = id;
            this.Title = title;
        }
        #endregion

        #region Properties
        public Guid Id { get; }
        public string Title { get; }
        #endregion
    }
}
