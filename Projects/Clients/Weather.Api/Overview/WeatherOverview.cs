﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Api.Overview
{
    public class WeatherData
    {
        #region Constructors
        public WeatherData(float temperature, float wind, string preciptitation, string state, TimeSpan time)
        {
            this.Temperature = temperature;
            this.Wind = wind;
            this.Precipitation = preciptitation;
            this.State = state;
            this.Time = time;
        }
        #endregion

        #region Properties
        public float Temperature { get; }
        public float Wind { get; }
        public string Precipitation { get; }
        public string State { get; }
        public TimeSpan Time { get; }
        #endregion
    }

    public class WeatherOverview
    {
        #region Constructors
        public WeatherOverview(WeatherData[] data)
        {
            this.Data = data;
        }
        #endregion

        #region Properties
        public IReadOnlyList<WeatherData> Data { get; }
        #endregion
    }
}
