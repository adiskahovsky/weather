﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

using Weather.Api.WeatherService;

using Weather.Api.Location;


namespace Weather.Api.Services
{
    public interface ILocationService
    {
        Task<IReadOnlyList<Town>> LoadTowns();
    }

    internal class LocationService : ILocationService
    {
        public async Task<IReadOnlyList<Town>> LoadTowns()
        {
            try
            {
                WeatherServiceClient client = new WeatherServiceClient();

                ResultOfTownModel_SPPP2DkU result = await client.GetTownsAsync();

                TownModel[] data = result.Data;
                if (data == null)
                    return null;

                return this.Convert(data);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private IReadOnlyList<Town> Convert(TownModel[] models)
        {
            List<Town> towns = new List<Town>(models.Length);

            foreach (TownModel model in models)
            {
                Town town = new Town(model.Id, model.Title);

                towns.Add(town);
            }

            return towns;
        }
    }
}
