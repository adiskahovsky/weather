﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Weather.Api.WeatherService;

using Weather.Api.Overview;


namespace Weather.Api.Services
{
    public interface IOverviewService
    {
        Task<WeatherOverview> LoadWeather(Guid townId, DateTime date);
    }

    internal class OverviewService : IOverviewService
    {
        public async Task<WeatherOverview> LoadWeather(Guid townId, DateTime date)
        {
            try
            {
                WeatherServiceClient client = new WeatherServiceClient();

                ResultOfOverviewModel_SPPP2DkU result = await client.GetOverviewAsync(townId, date);

                OverviewModel[] data = result.Data;

                OverviewModel model = data.FirstOrDefault();
                if (model == null)
                    return null;

                return this.Convert(model);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private WeatherOverview Convert(OverviewModel model)
        {
            WeatherData[] data = this.Convert(model.Data);

            WeatherOverview overview = new WeatherOverview(data);

            return overview;
        }
        private WeatherData[] Convert(DataModel[] models)
        {
            List<WeatherData> data = new List<WeatherData>(models.Length);

            foreach (DataModel model in models)
            {
                WeatherData item = new WeatherData(model.Temperature, model.Wind, model.Precipitation, model.State, model.Time);

                data.Add(item);
            }

            return data.ToArray();
        }
    }
}
