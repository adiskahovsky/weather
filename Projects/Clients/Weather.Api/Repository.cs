﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DryIoc;

using Weather.Api.Services;


namespace Weather.Api
{
    public static class Repository
    {
        static Repository()
        {
            container = Repository.Register();
        }

        private static Container container;

        public static T GetService<T>()
        {
            return container.Resolve<T>();
        }

        private static Container Register()
        {
            Container container = new Container();

            container.Register<ILocationService, LocationService>();
            container.Register<IOverviewService, OverviewService>();

            return container;
        }

    }
}
