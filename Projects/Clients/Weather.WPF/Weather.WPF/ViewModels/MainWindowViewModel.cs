﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using Weather.Api;
using Weather.Api.Location;
using Weather.Api.Overview;
using Weather.Api.Services;

using Weather.WPF.ViewModels.Base;


namespace Weather.WPF.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Constructors
        public MainWindowViewModel()
        {
            this.Setup();
        }
        #endregion

        #region Events
        private event EventHandler<Town> TownSelected;
        #endregion

        #region Fields
        private bool ready;

        private ObservableCollection<Town> towns;
        private Town selectedTown;

        private ObservableCollection<WeatherData> data;
        #endregion

        #region Properties
        public bool Ready
        {
            get { return this.ready; }
            set
            {
                this.ready = value;

                this.OnPropertyChanged(nameof(this.Ready));
            }
        }

        public ObservableCollection<Town> Towns
        {
            get { return this.towns; }
            set
            {
                this.towns = value;

                this.OnPropertyChanged(nameof(this.Towns));
            }
        }
        public Town SelectedTown
        {
            get { return this.selectedTown; }
            set
            {
                this.selectedTown = value;

                this.TownSelected(this, value);

                this.OnPropertyChanged(nameof(this.SelectedTown));
            }
        }

        public ObservableCollection<WeatherData> Data
        {
            get { return this.data; }
            set
            {
                this.data = value;

                this.OnPropertyChanged(nameof(this.Data));
            }
        }
        #endregion

        #region Handles
        private async void OnTownSelected(object sender, Town target)
        {
            this.Ready = false;

            Guid townId = target.Id;
            DateTime date = this.GetTomorrowDate();

            WeatherOverview weather = await this.GetWeather(townId, date);

            WeatherData[] data = weather.Data?.OrderBy(item=> item.Time).ToArray();
            this.UpdateData(data);

            this.Ready = true;
        }
        #endregion

        #region Assistants
        private async void Setup()
        {
            this.Ready = false;

            IReadOnlyList<Town> towns = await this.GetTowns();

            if (towns != null)
            {
                towns = towns.OrderBy(item => item.Title).ToArray();

                this.TownSelected += OnTownSelected;
                this.Towns = new ObservableCollection<Town>(towns);
                this.Data = new ObservableCollection<WeatherData>();
            }

            this.Ready = true;
        }

        private DateTime GetTomorrowDate()
        {
            return DateTime.Now.AddDays(1).Date;
        }

        private async Task<IReadOnlyList<Town>> GetTowns()
        {
            ILocationService location = Repository.GetService<ILocationService>();

            IReadOnlyList<Town> towns = await location.LoadTowns();

            return towns;
        }
        private async Task<WeatherOverview> GetWeather(Guid townId, DateTime date)
        {
            IOverviewService overview = Repository.GetService<IOverviewService>();

            WeatherOverview weather = await overview.LoadWeather(townId, date);

            return weather;
        }

        private void UpdateData(WeatherData[] data)
        {
            this.Data.Clear();

            if (data != null)
            {
                foreach (WeatherData item in data)
                {
                    this.Data.Add(item);
                }
            }
        }
        #endregion
    }
}
