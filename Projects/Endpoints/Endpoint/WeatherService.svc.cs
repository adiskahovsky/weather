﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using Models;

using Fasades.Location;
using Fasades.Overview;


namespace Endpoint
{
    public class WeatherService : IWeatherService
    {
        public async Task<Result<TownModel>> GetTowns()
        {
            Town.Retrieving retrieving = Town.Retrieving.New();

            IReadOnlyList<TownModel> result = await retrieving.Go();

            return Result<TownModel>.Many(result);
        }

        public async Task<Result<OverviewModel>> GetOverview(Guid townId, DateTime date)
        {
            WeatherOverview.Retrieving retrieving = WeatherOverview.Retrieving.New();
            retrieving.TownId = townId;
            retrieving.Date = date;

            OverviewModel result = await retrieving.Go();

            return Result<OverviewModel>.One(result);
        }
    }
}
