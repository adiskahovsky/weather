﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Threading.Tasks;

using Models;


namespace Endpoint
{
    [ServiceContract]
    public interface IWeatherService
    {
        [OperationContract]
        Task<Result<TownModel>> GetTowns();

        [OperationContract]
        Task<Result<OverviewModel>> GetOverview(Guid towId, DateTime date);
    }
}
