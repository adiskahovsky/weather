﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Transactions;

//using Common;

//using Provider.Gismeteo;
//using Provider.Gismeteo.Logic;

//using Database.Entities;

//using Repository.Towns;
//using Repository.Days;
//using Repository.WeatherDatas;

//using Base = Fasades.Gismeteo.Importing;


//namespace Logic
//{
//    using Data = List<(float Temperature, float Wind, string Precipitation, string State, TimeSpan Time)>;

//    public static partial class Gistemeo
//    {

//        public sealed class Importing : Base
//        {
//            #region Classes
//            private class Target
//            {
//                #region Static
//                public static implicit operator (float, float, string, string, TimeSpan) (Target weather)
//                {
//                    return (weather.Temperature, weather.Wind, weather.Precipitation, weather.State, weather.Time);
//                }
//                #endregion

//                #region Constructors
//                public Target(float temperature, float wind, string precipitation, string state, TimeSpan time)
//                {
//                    this.Temperature = temperature;
//                    this.Wind = wind;
//                    this.Precipitation = precipitation;
//                    this.State = state;
//                    this.Time = time;
//                }
//                #endregion

//                #region Properties
//                public float Temperature { get; }
//                public float Wind { get; }
//                public string Precipitation { get; }
//                public string State { get; }
//                public TimeSpan Time { get; }
//                #endregion
//            }

//            private class Targets : IEnumerable<Target>
//            {
//                #region Constructors
//                public Targets(IReadOnlyList<IWeather> weathers)
//                {
//                    this.Setup(weathers);
//                }
//                #endregion

//                #region Properties
//                private List<Target> Elements { get; } = new List<Target>();

//                public DateTime Date { get; private set; }

//                public int Count
//                {
//                    get { return this.Elements.Count; }
//                }
//                #endregion

//                #region IEnumerable
//                IEnumerator IEnumerable.GetEnumerator()
//                {
//                    return this.GetEnumerator();
//                }
//                #endregion

//                #region Methods
//                public IEnumerator<Target> GetEnumerator()
//                {
//                    return this.Elements.GetEnumerator();
//                }
//                #endregion

//                #region Assistants
//                private void Setup(IReadOnlyList<IWeather> weathers)
//                {
//                    this.SetupDate(weathers);

//                    foreach (IWeather weather in weathers)
//                    {
//                        float temperature = weather.Temperature;
//                        float wind = weather.Wind;
//                        string precipitation = weather.Precipitation;
//                        string state = weather.State;
//                        TimeSpan time = weather.Point.TimeOfDay;

//                        Target element = new Target(temperature, wind, precipitation, state, time);

//                        this.Elements.Add(element);
//                    }
//                }
//                private void SetupDate(IReadOnlyList<IWeather> weathers)
//                {
//                    IWeather element = weathers.First();

//                    DateTime date = element.Point.Date;
//                    if (weathers.All(item => item.Point.Date == date) == false)
//                        throw new Exception(nameof(weathers));

//                    this.Date = date;
//                }
//                #endregion
//            }
//            #endregion

//            #region Fields
//            private string town;
//            private string link;
//            #endregion

//            #region Overriding
//            public override string Town
//            {
//                get { return this.town; }
//                set { this.town = value; }
//            }
//            public override string Link
//            {
//                get { return this.link; }
//                set { this.link = value; }
//            }

//            public override async Task Go()
//            {
//                TransactionOptions options = new TransactionOptions();
//                options.IsolationLevel = IsolationLevel.Serializable;

//                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options))
//                {
//                    try
//                    {
//                        if (this.Verify() == false)
//                            throw new Exception("The facade was filled incorrectly.");

//                        await this.Process();
//                    }
//                    catch (Exception e)
//                    {
//                        Logging.Log(LogType.Error, e.Message);
//                    }
//                }
//            }
//            #endregion

//            #region Assistants
//            private bool Verify()
//            {
//                if (this.town == null)
//                    return false;

//                if (this.link == null)
//                    return false;

//                return true;
//            }

//            private async Task Process()
//            {
//                AccessLink access = new AccessLink();

//                await this.Process(access);
//            }
//            private async Task Process(AccessLink access)
//            {
//                //Targets
//                IReadOnlyList<IWeather> weathers = await access.GetWeathers(this.Link);

//                Targets targets = new Targets(weathers);

//                // Town
//                Town town = await this.ProcessTown(this.Town);

//                // Data
//                WeatherData[] data = await this.ProcessData(targets);

//                // Overview
//                WeatherOverview overview = new WeatherOverview(town, data);

//                // Day
//                await this.ProcessDay(targets, overview);
//            }
//            private async Task<Town> ProcessTown(string title)
//            {
//                Town town = new Town(title);

//                await this.ApplyTown(town);

//                return town;
//            }
//            private async Task<WeatherData[]> ProcessData(Targets targets)
//            {
//                Data data = new Data(targets.Count);

//                foreach (Target target in targets)
//                {
//                    data.Add(target);
//                }

//                WeatherData[] weathers = await this.ApplyData(data);

//                return weathers;

//            }
//            private async Task ProcessDay(Targets targets, WeatherOverview overview)
//            {
//                DateTime date = targets.Date;

//                await this.ApplyDay(date, overview);
//            }

//            private async Task ApplyTown(Town town)
//            {
//                Town entity = Towns.Fetching().ByTitle(town.Title).Go();
//                if (entity == null)
//                    await Towns.Managing().Insert(town);
//            }
//            private async Task<WeatherData[]> ApplyData(Data data)
//            {
//                WeatherData[] entities = WeatherDatas.Fetching().ByData(data).Go();
//                if (entities.Length == data.Count)
//                    return entities;

//                int capacity = data.Count - entities.Length;

//                List<WeatherData> weathers = new List<WeatherData>(capacity);

//                foreach ((float Temperature, float Wind, string Precipitation, string State, TimeSpan Time) value in data)
//                {
//                    if (entities.Any(item => item != value) == true)
//                    {
//                        float temperature = value.Temperature;
//                        float wind = value.Wind;
//                        string preciptitation = value.Precipitation;
//                        string state = value.State;
//                        TimeSpan time = value.Time;

//                        WeatherData weather = new WeatherData(temperature, wind, preciptitation, state, time);

//                        weathers.Add(weather);
//                    }
//                }

//                await WeatherDatas.Managing().Insert(weathers.ToArray());

//                weathers.AddRange(entities);

//                return weathers.ToArray();
//            }
//            private async Task ApplyDay(DateTime date, WeatherOverview overview)
//            {
//                Day entity = Days.Fetching().ByDate(date).Go();
//                if (entity == null)
//                {
//                    Day day = new Day(date, overview);

//                    await Days.Managing().Insert(day);
//                }
//            }
//            #endregion
//        }
//    }
//}
