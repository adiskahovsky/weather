﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

using Common;

//using Provider.Gismeteo;
//using Provider.Gismeteo.Logic;

using Base = Fasades.Gismeteo.Balancing;


namespace Logic
{
    public static partial class Gistemeo
    {
        public sealed class Balancing : Base
        {
            #region Overriding
            public override async Task<IReadOnlyList<(string Town, string Link)>> Go()
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.Serializable;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options))
                {
                    try
                    {
                        IReadOnlyList<(string Town, string Link)> result = await this.Process();

                        return result;
                    }
                    catch (Exception e)
                    {
                        Logging.Log(LogType.Error, e.Message);

                        return null;
                    }
                }
            }
            #endregion

            #region Assistants
            private async Task<IReadOnlyList<(string Town, string Link)>> Process()
            {
                //AccessLink access = new AccessLink();

                //IReadOnlyList<ITown> towns = await access.GetTowns();

                //return this.Convert(towns);

                return null;
            }

            //private IReadOnlyList<(string, string)> Convert(IReadOnlyList<ITown> towns)
            //{
            //    List<(string, string)> targets = new List<(string, string)>(towns.Count);

            //    foreach (ITown town in towns)
            //    {
            //        targets.Add((town.Title, town.Link));
            //    }

            //    return targets;
            //}
            #endregion
        }
    }
}
