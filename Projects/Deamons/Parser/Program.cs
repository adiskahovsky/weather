﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

using Fasades.Integration;


namespace Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            Execute().Wait();
        }

        static async Task Execute()
        {
            while (true)
            {
                Logging.Log(LogType.Info, $"Start {nameof(Gismeteo.Balancing)}", true);

                Gismeteo.Balancing balancing = Gismeteo.Balancing.New();

                IReadOnlyList<(string Town, string Link)> elements = await balancing.Go();

                Logging.Log(LogType.Info, $"End {nameof(Gismeteo.Balancing)}", true);

                Logging.Log(LogType.Info, $"Found {elements.Count} cities.", true);

                foreach ((string town, string link) in elements)
                {
                    Logging.Log(LogType.Info, $"Processing {link}", true);

                    Gismeteo.Importing importing = Gismeteo.Importing.New();
                    importing.Town = town;
                    importing.Link = link + "tomorrow/";

                    await importing.Go();
                }

                Logging.Log(LogType.Info, "All towns was processed.", true);

                await Task.Delay(10000); // TODO: In config.
            }
        }
    }
}
