﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Database.Entities
{
    [Table(nameof(WeatherData))]
    public class WeatherData : Entity
    {
        #region Static
        public static bool operator ==(WeatherData left, (float Temperature, float Wind, string Precipitation, string State, TimeSpan Time) right)
        {
            if (left.Temperature != right.Temperature)
                return false;

            if (left.Wind != right.Wind)
                return false;

            if (left.Precipitation != right.Precipitation)
                return false;

            if (left.State != right.State)
                return false;

            if (left.Time != right.Time)
                return false;

            return true;
        }
        public static bool operator !=(WeatherData left, (float Temperature, float Wind, string Precipitation, string State, TimeSpan Time) right)
        {
            return !(left == right);
        }
        #endregion

        #region Classes
        private static class Validation
        {
            public static bool Temperature(float value)
            {
                if (value < -100 || value > 50)
                    return false;

                return true;
            }
            public static bool Wind(float value)
            {
                if (value < 0 || value > 120)
                    return false;

                return true;
            }
            public static bool State(string value)
            {
                if (string.IsNullOrEmpty(value) == true)
                    return false;

                if (value.Length > 50)
                    return false;

                return true;
            }
            public static bool Time(TimeSpan time)
            {
                if (time.TotalHours > 24)
                    return false;

                return true;
            }
        }
        #endregion

        #region Constructors
        private WeatherData()
        {
        }

        public WeatherData(float temperature, float wind, string precipitation, string state, TimeSpan time)
        {
            this.Id = Guid.NewGuid();
            this.Temperature = temperature;
            this.Wind = wind;
            this.Precipitation = precipitation;
            this.State = state;
            this.Time = time;
        }
        #endregion

        #region Fields
        private float temperature;
        private float wind;
        private string state;
        private TimeSpan time;
        #endregion

        #region Properties
        public float Temperature
        {
            get { return this.temperature; }
            private set
            {
                if (Validation.Temperature(value) == false)
                    throw new ArgumentException(nameof(this.Temperature));

                this.temperature = value;
            }
        }
        public float Wind
        {
            get { return this.wind; }
            private set
            {
                if(Validation.Wind(value) == false)
                    throw new ArgumentException(nameof(this.Wind));

                this.wind = value;
            }
        }
        public string Precipitation { get; private set; }
        public string State
        {
            get { return this.state; }
            private set
            {
                if(Validation.State(value) == false)
                    throw new ArgumentException(nameof(this.State));

                this.state = value;
            }
        }
        public TimeSpan Time
        {
            get { return this.time; }
            private set
            {
                if (Validation.Time(value) == false)
                    throw new AccessViolationException(nameof(this.Time));

                this.time = value;
            }
        }
        public virtual ICollection<WeatherOverview> Overviews { get; private set; } = new List<WeatherOverview>();
        #endregion

        #region Overriding
        public override bool Equals(object obj)
        {
            return base.Equals(obj);

        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Methods
        public void Add(WeatherOverview overview)
        {
            this.Overviews.Add(overview);
        }
        public void Add(WeatherOverview[] overviews)
        {
            foreach (WeatherOverview overview in overviews)
            {
                this.Add(overview);
            }
        }
        #endregion
    }
}
