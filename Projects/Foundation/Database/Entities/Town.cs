﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Database.Entities
{
    public  class Town: Entity
    {
        #region Constructors
        private Town()
        {
        }

        public Town(string title)
        {
            this.Title = title;
            this.Id = Guid.NewGuid();
        }
        #endregion

        #region Properties
        public string Title { get; private set; }

        public virtual ICollection<WeatherOverview> Overviews { get; private set; } = new List<WeatherOverview>();
        #endregion

        #region Methods
        public void Add(WeatherOverview overview)
        {
            this.Overviews.Add(overview);
        }
        public void Add(WeatherOverview[] overviews)
        {
            foreach (WeatherOverview overview in overviews)
            {
                this.Add(overview);
            }
        }
        #endregion
    }
}
