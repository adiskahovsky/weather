﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Database.Entities
{
    public class WeatherOverview : Entity
    {
        #region Constructors
        private WeatherOverview()
        {
        }

        public WeatherOverview(Guid dayId, Guid townId)
        {
            this.Id = Guid.NewGuid();
            this.DayId = dayId;
            this.TownId = townId;
        }
        #endregion

        #region Properties
        public Guid DayId { get; private set; }
        public Guid TownId { get; private set; }

        public virtual Day Day { get; set; }
        public virtual Town Town { get; private set; }
        public virtual ICollection<WeatherData> Data { get; private set; } = new List<WeatherData>();
        #endregion

        #region Methods
        public void Add(WeatherData element)
        {
            this.Data.Add(element);
        }
        public void Add(WeatherData[] elements)
        {
            foreach (WeatherData element in elements)
            {
                this.Add(element);
            }
        }
        #endregion
    }
}
