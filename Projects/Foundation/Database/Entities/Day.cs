﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Database.Entities
{
    public class Day : Entity
    {
        #region Classes
        private static class Validation
        {
            public static bool Date(DateTime value)
            {
                if (value != value.Date)
                    return false;

                return true;
            }
        }
        #endregion

        #region Constructors
        private Day()
        {
        }

        public Day(DateTime date)
        {
            this.Id = Guid.NewGuid();
            this.Date = date;
        }
        #endregion

        #region Fields
        private DateTime date;
        #endregion

        #region Properties
        public DateTime Date
        {
            get { return this.date; }
            private set
            {
                if (Validation.Date(value) == false)
                    throw new ArgumentException(nameof(this.Date));

                this.date = value;
            }
        }

        public virtual ICollection<WeatherOverview> Overviews { get; private set; } = new List<WeatherOverview>();
        #endregion

        #region Methods
        public void Add(WeatherOverview overview)
        {
            this.Overviews.Add(overview);
        }
        public void Add(WeatherOverview[] overviews)
        {
            foreach (WeatherOverview overview in overviews)
            {
                this.Add(overview);
            }
        }
        #endregion
    }
}
