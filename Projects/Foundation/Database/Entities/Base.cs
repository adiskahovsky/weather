﻿using System;


namespace Database.Entities
{
    public abstract class Entity
    {
        public Guid Id { get; protected set; }
    }
}
