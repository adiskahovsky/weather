﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Database.Entities;


namespace Database.Contexts
{
    public class Context : DbContext
    {
        public Context() : base("WeatherDb")
        {
        }

        public DbSet<Day> Days { get; set; }
        public DbSet<Town> Towns { get; set; }
        public DbSet<WeatherData> WeatherData { get; set; }
        public DbSet<WeatherOverview> WeatherOverviews { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Day>().HasMany(item => item.Overviews).WithRequired(item => item.Day).HasForeignKey(item => item.DayId);
            modelBuilder.Entity<Town>().HasMany(item => item.Overviews).WithRequired(item => item.Town).HasForeignKey(item => item.TownId);
            modelBuilder.Entity<WeatherOverview>().HasMany(item => item.Data).WithMany(item => item.Overviews).Map(
                item =>
                {
                    item.ToTable("Weathers");

                    item.MapLeftKey("OverviewId");
                    item.MapRightKey("DataId"); 
                });

            base.OnModelCreating(modelBuilder);
        }

    }
}
