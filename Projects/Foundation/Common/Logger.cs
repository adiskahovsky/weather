﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;


namespace Common
{
    public enum LogType
    {
        Info = 1,
        Error
    }

    public static class Logging
    {
        private static Logger Logger { get; } = LogManager.GetCurrentClassLogger();

        public static void Log(LogType type, string message, bool console = false)
        {
            switch (type)
            {
                case LogType.Info:
                    Logger.Log(LogLevel.Info, message);
                    break;

                case LogType.Error:
                    Logger.Log(LogLevel.Error, message);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }

            if (console == true)
                Console.WriteLine(message);
        }
    }
}
