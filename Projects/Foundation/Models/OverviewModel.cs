﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace Models
{
    [DataContract]
    public class DataModel
    {
        #region Constructors
        public DataModel(float temperature, float wind, string preciptitation, string state, TimeSpan time)
        {
            this.Temperature = temperature;
            this.Wind = wind;
            this.Precipitation = preciptitation;
            this.State = state;
            this.Time = time;
        }
        #endregion

        #region Properties
        [DataMember]
        public float Temperature { get; set; }

        [DataMember]
        public float Wind { get; set; }

        [DataMember]
        public string Precipitation { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public TimeSpan Time { get; set; }
        #endregion
    }

    [DataContract]
    public class OverviewModel
    {
        public OverviewModel(DataModel[] data)
        {
            this.Data = data;
        }

        [DataMember]
        public DataModel[] Data { get; set; }   
    }
}
