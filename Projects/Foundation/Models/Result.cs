﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace Models
{
    [DataContract]
    public class Result<T>
    {
        private Result(T one)
        {
            T[] data = new T[] { one };

            this.Data = data;
        }
        private Result(T[] many)
        {
            this.Data = many;
        }

        private T[] data;

        [DataMember]
        public T[] Data
        {
            get{ return this.data; }
            private set { this.data = value; }
        }

        public static Result<T> One(T one)
        {
            Result<T> result = new Result<T>(one);

            return result;
        }
        public static Result<T> Many(T[] many)
        {
            Result<T> result = new Result<T>(many);

            return result;
        }
        public static Result<T> Many(IReadOnlyList<T> many)
        {
            Result<T> result = new Result<T>(many.ToArray());

            return result;
        }
    }
}
