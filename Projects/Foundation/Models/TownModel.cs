﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace Models
{
    [DataContract]
    public class TownModel 
    {
        #region Constructors
        public TownModel(Guid id, string title)
        {
            this.Id = id;
            this.Title = title;
        }
        #endregion

        #region Fields
        private Guid id;
        private string title;
        #endregion

        #region Properties
        [DataMember]
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }
        #endregion
    }
}
