﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Database.Entities;
using Database.Contexts;

using Entity = Database.Entities.WeatherOverview;


namespace Repository.WeatherOverviews
{
    public class Managing
    {
        public async Task Insert(Entity entity)
        {
            using (Context context = new Context())
            {
                context.WeatherOverviews.Add(entity);

                await context.SaveChangesAsync();
            }
        }
        public async Task Insert(Entity[] entities)
        {
            using (Context context = new Context())
            {
                context.WeatherOverviews.AddRange(entities);

                await context.SaveChangesAsync();
            }
        }

        public async Task Insert(Entity overview, WeatherData[] data)
        {
            using (Context context = new Context())
            {
                Guid[] dataIds = data.Select(item => item.Id).ToArray();

                IQueryable<WeatherData> entities = context.WeatherData.Where(item => dataIds.Contains(item.Id));

                foreach (WeatherData entitiy in entities)
                {
                    entitiy.Add(overview);
                }

                await context.SaveChangesAsync();
            }
        }
    }
}
