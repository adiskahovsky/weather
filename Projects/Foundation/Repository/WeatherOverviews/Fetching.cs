﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Database.Contexts;
using Database.Entities;

using Repository.Common;

using Entity = Database.Entities.WeatherOverview;


namespace Repository.WeatherOverviews
{
    public interface IDayTown
    {
        IMine<Entity> ByTown(Town town);
    }

    public class Fetching : Fetching<Entity>, IDayTown, IMine<Entity>, IMany<Entity>
    {
        #region Constructors
        internal Fetching()
        {
        }
        #endregion

        #region Properties
        private Day Day { get; set; }
        private Town Town { get; set; }
        #endregion

        #region IDay
        IMine<Entity> IDayTown.ByTown(Town town)
        {
            this.Town = town;

            return this;
        }
        #endregion

        #region IMine<Entity>
        async Task<Entity> IMine<Entity>.Go()
        {
            if (this.Id != null)
            {
                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Day != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Town != null)
                    throw new Exception(nameof(this.Ids));

                return await this.Mine();
            }

            if (this.Day != null && this.Town != null)
            {
                if(this.Id != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                using (Context context = new Context())
                {
                    Entity entity = await context.WeatherOverviews.FirstOrDefaultAsync(item=> item.DayId == this.Day.Id && 
                                                                                    item.TownId == this.Town.Id);

                    return entity;  
                }
            }

            return null;
        }
        #endregion

        #region IMany<Entity>
        async Task<Entity[]> IMany<Entity>.Go()
        {
            if (this.Ids != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                return await this.Many();
            }

            return null;
        }
        #endregion

        #region Overriding
        protected async override Task<Entity> Mine()
        {
            using (Context context = new Context())
            {
                Entity contry = await context.WeatherOverviews.FirstOrDefaultAsync(item => item.Id == this.Id);

                return contry;
            }
        }
        protected async override Task<Entity[]> Many()
        {
            using (Context context = new Context())
            {
                IQueryable<Entity> queryable = context.WeatherOverviews.Where(item => this.Ids.Contains(item.Id));

                return await queryable.ToArrayAsync();
            }
        }
        #endregion

        #region Methods
        public IDayTown ByDay(Day day)
        {
            this.Day = day;

            return this;
        }
        #endregion
    }
}
