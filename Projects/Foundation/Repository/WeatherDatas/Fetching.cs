﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Database.Entities;
using Database.Contexts;

using Repository.Common;

using Entity = Database.Entities.WeatherData;


namespace Repository.WeatherDatas
{
    using Data = List<(float Temperature, float Wind, string Precipitation, string State, TimeSpan Time)>;

    public class Fetching : Fetching<Entity>, IMine<Entity>, IMany<Entity>
    {
        #region Constructors
        internal Fetching()
        {
        }
        #endregion

        #region Properties
        private Guid? OverviewId { get; set; }
        private Data Data { get; set; }
        #endregion

        #region IMine<Entity>
        async Task<Entity> IMine<Entity>.Go()
        {
            if (this.Id != null)
            {
                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                return await this.Mine();
            }

            return null;
        }
        #endregion

        #region IMany<Entity>
        async Task<Entity[]> IMany<Entity>.Go()
        {
            if (this.Ids != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.OverviewId != null)
                    throw new Exception(nameof(this.OverviewId));

                if (this.Data != null)
                    throw new Exception(nameof(this.Data));

                return await this.Many();
            }

            if (this.OverviewId != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                using (Context context = new Context())
                {
                    WeatherOverview overview = await context.WeatherOverviews.Include(item=> item.Data).FirstOrDefaultAsync(item=> item.Id == this.OverviewId);
                    if (overview == null)
                        return null;

                    return overview.Data.ToArray();
                }
            }

            if (this.Data != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                using (Context context = new Context())
                {
                    List<Entity> entities = new List<Entity>();

                    IQueryable<Entity> queryable = context.WeatherData;

                    foreach ((float temperature, float wind, string precipitation, string state, TimeSpan time) in this.Data)
                    {
                        Entity entity = await queryable.FirstOrDefaultAsync(item => item.Temperature == temperature
                                                                && item.Wind == wind
                                                                && item.Precipitation == precipitation
                                                                && item.State == state
                                                                && item.Time == time);
                        if(entity != null)
                            entities.Add(entity);
                    }

                    return entities.ToArray();
                }
            }

            return null;
        }
        #endregion

        #region Overriding
        protected async override Task<Entity> Mine()
        {
            using (Context context = new Context())
            {
                Entity contry = await context.WeatherData.FirstOrDefaultAsync(item => item.Id == this.Id);

                return contry;
            }
        }
        protected async override Task<Entity[]> Many()
        {
            using (Context context = new Context())
            {
                IQueryable<Entity> queryable = context.WeatherData.Where(item => this.Ids.Contains(item.Id));

                return await queryable.ToArrayAsync();
            }
        }
        #endregion

        #region Methods
        public IMany<Entity> ByOverviewId(Guid overviewId)
        {
            this.OverviewId = overviewId;

            return this;
        }
        public IMany<Entity> ByData(Data data)
        {
            this.Data = data;

            return this;
        }
        #endregion
    }
}
