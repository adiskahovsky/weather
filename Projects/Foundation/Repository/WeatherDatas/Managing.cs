﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Database.Contexts;

using Entity = Database.Entities.WeatherData;


namespace Repository.WeatherDatas
{
    public class Managing
    {
        public async Task Insert(Entity entity)
        {
            using (Context context = new Context())
            {
                context.WeatherData.Add(entity);

                await context.SaveChangesAsync();
            }
        }

        public async Task Insert(Entity[] entities)
        {
            using (Context context = new Context())
            {
                context.WeatherData.AddRange(entities);

                await context.SaveChangesAsync();
            }
        }
    }
}
