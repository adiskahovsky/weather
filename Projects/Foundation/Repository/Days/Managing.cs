﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Database.Contexts;

using Entity = Database.Entities.Day;


namespace Repository.Days
{
    public class Managing
    {
        public async Task Insert(Entity entity)
        {
            using (Context context = new Context())
            {
                context.Days.Add(entity);

                await context.SaveChangesAsync();
            }
        }
        public async Task Insert(Entity[] entities)
        {
            using (Context context = new Context())
            {
                context.Days.AddRange(entities);

                await context.SaveChangesAsync();
            }
        }

        public async Task Remove(Entity entity)
        {
            using (Context context = new Context())
            {
                context.Days.Remove(entity);

                await context.SaveChangesAsync();
            }
        }
        public async Task Remove(Entity[] entities)
        {
            using (Context context = new Context())
            {
                context.Days.RemoveRange(entities);

                await context.SaveChangesAsync();
            }
        }
    }
}
