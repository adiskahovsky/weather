﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Database.Contexts;

using Repository.Common;

using Entity = Database.Entities.Day;


namespace Repository.Days
{
    public class Fetching : Fetching<Entity>, IMine<Entity>, IMany<Entity>
    {
        #region Constructors
        internal Fetching()
        {
        }
        #endregion

        #region Properties
        private DateTime? Date { get; set; }
        private DateTime[] Dates{ get; set; }
        #endregion

        #region IMine<Entity>
        async Task<Entity> IMine<Entity>.Go()
        {
            if (this.Id != null)
            {
                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Date != null)
                    throw new Exception(nameof(this.Date));

                if (this.Dates != null)
                    throw new Exception(nameof(this.Dates));

                return await this.Mine();
            }

            if (this.Date != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Dates != null)
                    throw new Exception(nameof(this.Dates));

                using (Context context = new Context())
                {
                    Entity date = await context.Days.FirstOrDefaultAsync(item => item.Date == this.Date);

                    return date;
                }
            }

            return null;
        }
        #endregion

        #region IMany<Entity>
        async Task<Entity[]> IMany<Entity>.Go()
        {
            if (this.Ids != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Date != null)
                    throw new Exception(nameof(this.Date));

                if (this.Dates != null)
                    throw new Exception(nameof(this.Dates));

                return await this.Many();
            }

            if (this.Dates != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Date != null)
                    throw new Exception(nameof(this.Date));

                    using (Context context = new Context())
                    {
                        IQueryable<Entity>  queryable = context.Days.Where(item=> this.Dates.Contains(item.Date));

                        return await queryable.ToArrayAsync();
                    }
            }

            return null;
        }
        #endregion

        #region Overriding
        protected async override Task<Entity> Mine()
        {
            using (Context context = new Context())
            {
                Entity contry = await context.Days.FirstOrDefaultAsync(item => item.Id == this.Id);

                return contry;
            }
        }
        protected async override Task<Entity[]> Many()
        {
            using (Context context = new Context())
            {
                IQueryable<Entity> queryable = context.Days.Where(item => this.Ids.Contains(item.Id));

                return await queryable.ToArrayAsync();
            }
        }
        #endregion

        #region Methods
        public IMine<Entity> ByDate(DateTime date)
        {
            this.Date = date;

            return this;
        }
        public IMany<Entity> ByDates(DateTime[] dates)
        {
            this.Dates = dates;

            return this;
        }
        #endregion
    }
}
