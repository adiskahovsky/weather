﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Database.Contexts;

using Repository.Common;

using Entity = Database.Entities.Town;


namespace Repository.Towns
{
    public class Fetching : Fetching<Entity>, IMine<Entity>, IMany<Entity>
    {
        #region Constructors
        internal Fetching()
        {
        }
        #endregion

        #region Properties
        private string Title { get; set; }
        private string[] Titles { get; set; }
        #endregion

        #region IMine<Entity>
        async Task<Entity> IMine<Entity>.Go()
        {
            if (this.Id != null)
            {
                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Title != null)
                    throw new Exception(nameof(this.Title));

                if (this.Titles != null)
                    throw new Exception(nameof(this.Titles));

                return await this.Mine();
            }

            if (this.Title != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Titles != null)
                    throw new Exception(nameof(this.Titles));

                using (Context context = new Context())
                {
                    Entity contry = await context.Towns.FirstOrDefaultAsync(item => item.Title == this.Title);

                    return contry;
                }
            }

            return null;
        }
        #endregion

        #region IMany<Entity>
        async Task<Entity[]> IMany<Entity>.Go()
        {
            if (this.Ids != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Title != null)
                    throw new Exception(nameof(this.Title));

                if (this.Titles != null)
                    throw new Exception(nameof(this.Titles));

                return await this.Many();
            }

            if (this.Titles != null)
            {
                if (this.Id != null)
                    throw new Exception(nameof(this.Id));

                if (this.Ids != null)
                    throw new Exception(nameof(this.Ids));

                if (this.Title != null)
                    throw new Exception(nameof(this.Title));

                using (Context context = new Context())
                {
                    IQueryable<Entity> queryable = context.Towns.Where(item=> this.Titles.Contains(item.Title));

                    return await queryable.ToArrayAsync();
                }
            }

            return null;
        }
        #endregion

        #region Overriding
        protected async override Task<Entity> Mine()
        {
            using (Context context = new Context())
            {
                Entity contry = await context.Towns.FirstOrDefaultAsync(item => item.Id == this.Id);

                return contry;
            }
        }
        protected async override Task<Entity[]> Many()
        {
            using (Context context = new Context())
            {
                IQueryable<Entity> queryable = context.Towns.Where(item => this.Ids.Contains(item.Id));

                return await queryable.ToArrayAsync();
            }
        }
        #endregion

        #region Methods
        public async Task<Entity[]> All()
        {
            using (Context context = new Context())
            {
                return await context.Towns.ToArrayAsync();
            }
        }

        public IMine<Entity> ByTitle(string title)
        {
            this.Title = title;

            return this;
        }
        public IMany<Entity> ByTitles(string[] titles)
        {
            this.Titles = titles;

            return this;
        }
        #endregion
    }
}
