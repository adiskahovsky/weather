﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Database.Contexts;

using Entity = Database.Entities.Town;


namespace Repository.Towns
{
    public class Managing
    {
        public async Task Insert(Entity entity)
        {
            using (Context context = new Context())
            {
                context.Towns.Add(entity);

                await context.SaveChangesAsync();
            }
        }

        public async Task Insert(Entity[] entities)
        {
            using (Context context = new Context())
            {
                context.Towns.AddRange(entities);

                await context.SaveChangesAsync();
            }
        }
    }
}
