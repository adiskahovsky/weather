﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Repository.Common
{
    public interface IMany<T>
    {
        Task<T[]> Go();
    }
}
