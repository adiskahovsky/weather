﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Database.Contexts;


namespace Repository.Common
{
    public abstract class Fetching<T> : IMine<T>, IMany<T>
    {
        protected Guid? Id { get; set; }
        protected Guid[] Ids { get; set; }

        public IMine<T> ById(Guid id)
        {
            this.Id = id;

            return this;
        }
        public IMany<T> ByIds(Guid[] ids)
        {
            this.Ids = ids;

            return this;
        }

        async Task<T> IMine<T>.Go()
        {
            return await this.Mine();
        }
        async Task<T[]> IMany<T>.Go()
        {
            return await this.Many();
        }

        protected async virtual Task<T> Mine()
        {
            IMine<T> mine = this;

            return await mine.Go();
        }
        protected async virtual Task<T[]> Many()
        {
            IMany<T> many = this;

            return await many.Go();
        }
    }
}
