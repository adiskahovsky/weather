﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

using Common;

using Database.Entities;

using Repository.Days;
using Repository.Towns;
using Repository.WeatherDatas;
using Repository.WeatherOverviews;

using Models;

using Base = Fasades.Overview.WeatherOverview.Retrieving;


namespace Logic.Overview
{
    internal static partial class WeatherOverview
    {
        public sealed class Retrieving : Base
        {
            #region Classes
            private class Generation
            {
                #region Constructors
                public Generation(IReadOnlyList<WeatherData> data)
                {
                    this.Data = data;
                }
                #endregion

                #region Properties
                private IReadOnlyList<WeatherData> Data { get; }
                #endregion

                #region Methods
                public OverviewModel Run()
                {
                    // Data
                    DataModel[] data = this.GenerateData(this.Data);

                    // Overview
                    OverviewModel overview = new OverviewModel(data);

                    return overview;
                }
                #endregion

                #region Assistants
                private DataModel[] GenerateData(IReadOnlyList<WeatherData> sources)
                {
                    List<DataModel> targets = new List<DataModel>(sources.Count);

                    foreach (WeatherData source in sources)
                    {
                        DataModel target = new DataModel(source.Temperature, source.Wind, source.Precipitation, source.State, source.Time);

                        targets.Add(target);
                    }

                    return targets.ToArray();
                }
                #endregion
            }
            #endregion

            #region Fields
            private Guid? townId;
            private DateTime? date;
            #endregion

            #region Overriding
            public override Guid TownId
            {
                get { return this.townId.Value; }
                set { this.townId = value; }
            }
            public override DateTime Date
            {
                get { return this.date.Value; }
                set { this.date = value; }
            }

            public override async Task<OverviewModel> Go()
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.Serializable;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        if (this.Verify() == false)
                            return null;

                        OverviewModel result = await this.Process();

                        scope.Complete();

                        return result;
                    }
                    catch (Exception e)
                    {
                        Logging.Log(LogType.Error, e.Message);

                        return null;
                    }
                }
            }
            #endregion

            #region Assistants
            private bool Verify()
            {
                if (this.townId == null)
                    return false;

                if (this.date == null)
                    return false;

                if (this.date != date.Value.Date)
                    return false;

                return true;
            }

            private async Task<OverviewModel> Process()
            {
                // Day
                Day day = await Days.Fetching().ByDate(this.Date).Go();
                if (day == null)
                    return null;

                // Town
                Town town = await Towns.Fetching().ById(this.TownId).Go();
                if (town == null)
                    return null;

                // Entity
                Entity entity = await WeatherOverviews.Fetching().ByDay(day).ByTown(town).Go();
                if (entity == null)
                    return null;

                // Data
                WeatherData[] data = await WeatherDatas.Fetching().ByOverviewId(entity.Id).Go();
    
                // Generation
                Generation generation = new Generation(data);

                // Overview
                OverviewModel overview = generation.Run();

                return overview;
            }
            #endregion
        }
    }
}
