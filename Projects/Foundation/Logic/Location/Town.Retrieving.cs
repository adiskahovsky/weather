﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

using Common;

using Repository.Towns;

using Models;

using Entity = Database.Entities.Town;

using Base = Fasades.Location.Town.Retrieving;


namespace Logic.Location
{
    internal static partial class Town
    {
        public sealed class Retrieving : Base
        {
            #region Overriding
            public override async Task<IReadOnlyList<TownModel>> Go()
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.Serializable;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        IReadOnlyList<TownModel> result = await this.Process();

                        scope.Complete();

                        return result;
                    }
                    catch (Exception e)
                    {
                        Logging.Log(LogType.Error, e.Message);

                        return null;
                    }
                }
            }
            #endregion

            #region Assistants
            private async Task<IReadOnlyList<TownModel>> Process()
            {
                Entity[] entities = await Repository.Towns.Towns.Fetching().All();

                List<TownModel> towns = new List<TownModel>(entities.Length);

                foreach (Entity entity in entities)
                {
                    TownModel town = new TownModel(entity.Id, entity.Title);

                    towns.Add(town);
                }

                return towns;
            }
            #endregion
        }
    }
}
