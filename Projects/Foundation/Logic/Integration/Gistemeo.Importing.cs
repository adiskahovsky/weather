﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

using Common;

using Provider.Gismeteo;
using Provider.Gismeteo.Logic;

using Database.Entities;

using Repository.Days;
using Repository.Towns;
using Repository.WeatherDatas;
using Repository.WeatherOverviews;

using Base = Fasades.Integration.Gismeteo.Importing;


namespace Logic.Integration
{
    using Data = List<(float Temperature, float Wind, string Precipitation, string State, TimeSpan Time)>;

    internal static partial class Gistemeo
    {
        public sealed class Importing : Base
        {
            #region Classes
            private class Target
            {
                #region Static
                public static implicit operator (float, float, string, string, TimeSpan) (Target weather)
                {
                    return (weather.Temperature, weather.Wind, weather.Precipitation, weather.State, weather.Time);
                }
                #endregion

                #region Constructors
                public Target(float temperature, float wind, string precipitation, string state, TimeSpan time)
                {
                    this.Temperature = temperature;
                    this.Wind = wind;
                    this.Precipitation = precipitation;
                    this.State = state;
                    this.Time = time;
                }
                #endregion

                #region Properties
                public float Temperature { get; }
                public float Wind { get; }
                public string Precipitation { get; }
                public string State { get; }
                public TimeSpan Time { get; }
                #endregion
            }

            private class Targets : IEnumerable<Target>
            {
                #region Constructors
                public Targets(IReadOnlyList<IWeather> weathers)
                {
                    this.Setup(weathers);
                }
                #endregion

                #region Properties
                private List<Target> Elements { get; } = new List<Target>();

                public DateTime Date { get; private set; }

                public int Count
                {
                    get { return this.Elements.Count; }
                }
                #endregion

                #region IEnumerable
                IEnumerator IEnumerable.GetEnumerator()
                {
                    return this.GetEnumerator();
                }
                #endregion

                #region Methods
                public IEnumerator<Target> GetEnumerator()
                {
                    return this.Elements.GetEnumerator();
                }
                #endregion

                #region Assistants
                private void Setup(IReadOnlyList<IWeather> weathers)
                {
                    this.SetupDate(weathers);

                    foreach (IWeather weather in weathers)
                    {
                        float temperature = weather.Temperature;
                        float wind = weather.Wind;
                        string precipitation = weather.Precipitation;
                        string state = weather.State;
                        TimeSpan time = weather.Point.TimeOfDay;

                        Target element = new Target(temperature, wind, precipitation, state, time);

                        this.Elements.Add(element);
                    }
                }
                private void SetupDate(IReadOnlyList<IWeather> weathers)
                {
                    IWeather element = weathers.First();

                    DateTime date = element.Point.Date;
                    //if (weathers.All(item => item.Point.Date == date) == false)
                    //    throw new Exception(nameof(weathers));

                    this.Date = date;
                }
                #endregion
            }
            #endregion

            #region Fields
            private string town;
            private string link;
            #endregion

            #region Overriding
            public override string Town
            {
                get { return this.town; }
                set { this.town = value; }
            }
            public override string Link
            {
                get { return this.link; }
                set { this.link = value; }
            }

            public override async Task Go()
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.Serializable;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        if (this.Verify() == false)
                            throw new Exception("The facade was filled incorrectly.");

                        await this.Process();

                        scope.Complete();
                    }
                    catch (Exception e)
                    {
                        Logging.Log(LogType.Error, e.Message);
                    }
                }
            }
            #endregion

            #region Assistants
            private bool Verify()
            {
                if (this.town == null)
                    return false;

                if (this.link == null)
                    return false;

                return true;
            }

            private async Task Process()
            {
                // Access
                AccessLink access = new AccessLink();

                //Targets
                IReadOnlyList<IWeather> weathers = await access.GetWeathers(this.Link);

                Targets targets = new Targets(weathers);

                // Day
                Day day = await this.GetDay(targets.Date);

                // Town
                Town town = await this.GetTown(this.Town);

                // Data
                WeatherData[] data = await this.GetData(targets);

                // Overview
                WeatherOverview overview = new WeatherOverview(day.Id, town.Id);

                // Applying
                await this.Apply(overview, data, day, town);
            }

            private async Task<Day> GetDay(DateTime date)
            {
                Day day = await Days.Fetching().ByDate(date).Go();
                if (day == null)
                {
                    day = new Day(date);

                    await Days.Managing().Insert(day);
                }

                return day;
            }
            private async Task<Town> GetTown(string title)
            {
                Town town = await Towns.Fetching().ByTitle(title).Go();
                if (town == null)
                {
                    town = new Town(title);

                    await Towns.Managing().Insert(town);
                }

                return town;
            }
            private async Task<WeatherData[]> GetData(Targets targets)
            {
                Data data = new Data(targets.Count);

                foreach (Target target in targets)
                {
                    data.Add(target);
                }

                WeatherData[] weathers = await this.GetData(data);

                return weathers;
            }
            private async Task<WeatherData[]> GetData(Data targets)
            {
                WeatherData[] entities = await WeatherDatas.Fetching().ByData(targets).Go();
                if (entities.Length == targets.Count)
                    return entities;

                int capacity = targets.Count - entities.Length;

                List<WeatherData> weathers = new List<WeatherData>(capacity);

                foreach ((float Temperature, float Wind, string Precipitation, string State, TimeSpan Time) target in targets)
                {
                    if (entities.Any(item => item == target) == false)
                    {
                        float temperature = target.Temperature;
                        float wind = target.Wind;
                        string preciptitation = target.Precipitation;
                        string state = target.State;
                        TimeSpan time = target.Time;

                        WeatherData weather = new WeatherData(temperature, wind, preciptitation, state, time);

                        weathers.Add(weather);
                    }
                }

                await WeatherDatas.Managing().Insert(weathers.ToArray());

                weathers.AddRange(entities);

                return weathers.ToArray();
            }

            private async Task Apply(WeatherOverview overview, WeatherData[] data, Day day, Town town)
            {
                WeatherOverview entity = await WeatherOverviews.Fetching().ByDay(day).ByTown(town).Go();
                if (entity == null)
                    await WeatherOverviews.Managing().Insert(overview, data);
            }
            #endregion
        }
    }
}
