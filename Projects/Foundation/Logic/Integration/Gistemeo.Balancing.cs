﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

using Common;

using Provider.Gismeteo;
using Provider.Gismeteo.Logic;

using Base = Fasades.Integration.Gismeteo.Balancing;


namespace Logic.Integration
{
    internal static partial class Gistemeo
    {
        public sealed class Balancing : Base
        {
            #region Overriding
            public override async Task<IReadOnlyList<(string Town, string Link)>> Go()
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.Serializable;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        IReadOnlyList<(string Town, string Link)> result = await this.Process();

                        scope.Complete();

                        return result;
                    }
                    catch (Exception e)
                    {
                        Logging.Log(LogType.Error, e.Message);

                        return null;
                    }
                }
            }
            #endregion

            #region Assistants
            private async Task<IReadOnlyList<(string Town, string Link)>> Process()
            {
                AccessLink access = new AccessLink();

                IReadOnlyList<ITown> towns = await access.GetTowns();

                return this.Convert(towns);
            }

            private IReadOnlyList<(string, string)> Convert(IReadOnlyList<ITown> towns)
            {
                List<(string, string)> targets = new List<(string, string)>(towns.Count);

                foreach (ITown town in towns)
                {
                    targets.Add((town.Title, town.Link));
                }

                return targets;
            }
            #endregion
        }
    }
}
