﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fasades.Common;


namespace Fasades.Integration
{
    public static partial class Gismeteo
    {
        public abstract class Importing 
        {
            #region Static
            public static Importing New()
            {
                return Facade.Get<Importing>();
            }
            #endregion

            #region Properties
            public abstract string Town { get; set; }
            public abstract string Link { get; set; }
            #endregion

            #region Methods
            public abstract Task Go();
            #endregion
        }
    }
}
