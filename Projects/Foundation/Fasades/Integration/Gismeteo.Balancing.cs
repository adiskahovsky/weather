﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fasades.Common;


namespace Fasades.Integration
{
    public static partial class Gismeteo
    {
        public abstract class Balancing 
        {
            #region Static
            public static Balancing New()
            {
                return Facade.Get<Balancing>();
            }
            #endregion

            #region Methods
            public abstract Task<IReadOnlyList<(string Town, string Link)>> Go();
            #endregion
        }
    }
}
