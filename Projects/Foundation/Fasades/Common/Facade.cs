﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using DryIoc;


namespace Fasades.Common
{
    public static class Facade
    {
        #region Classes
        private class Helper
        {
            #region Constructors
            public Helper()
            {
                this.Setup();
            }
            #endregion

            #region Properties
            private Container Container { get; set; }
            #endregion

            #region Methods
            public Container GetContainer()
            {
                return this.Container;
            }
            #endregion

            #region Assistants
            private void Setup()
            {
                string directory = this.GetSolutionDirectory();

                string[] files = Directory.GetFiles(directory, "Logic.dll", SearchOption.AllDirectories);

                string file = files.FirstOrDefault();
                if (file == null)
                    throw new Exception("Cannot find Logic.dll.");

                Assembly assembly = Assembly.GetExecutingAssembly();

                Type[] heirs = this.GetTypes(file);
                Type[] parents = this.GetTypes(assembly);

                (Type Parent, Type Heir)[] types = this.Map(parents, heirs);

                Container container = this.Register(types);

                this.Container = container;
            }

            private Container Register((Type Parent, Type Heir)[] types)
            {
                Container container = new Container();

                foreach ((Type parent, Type heir) in types)
                {
                    container.Register(parent, heir);
                }

                return container;
            }

            private Type[] GetTypes(string path)
            {
                if (File.Exists(path) == false)
                    throw new Exception($"File {path} not exists.");

                Assembly assembly = Assembly.LoadFile(path);
                Type[] types = assembly.GetLoadedTypes();
                //Type[] types = assembly.GetTypes();

                return types;
            }
            private Type[] GetTypes(Assembly assembly)
            {
                Type[] types = assembly.GetTypes().Where(item => item.IsAbstract == true).ToArray();

                return types;
            }

            private (Type Parent, Type Heir)[] Map(Type[] parents, Type[] heirs)
            {
                List<(Type Parent, Type Heir)> types = new List<(Type Parent, Type Heir)>(parents.Length);

                foreach (Type parent in parents)
                {
                    Type heir = heirs.FirstOrDefault(item => item.BaseType == parent);
                    if (heir != null)
                        types.Add((parent, heir));
                }

                return types.ToArray();
            }

            private string GetSolutionDirectory()
            {
                string directory = Assembly.GetExecutingAssembly().Location;

                DirectoryInfo parent = Directory.GetParent(directory);

                string path = parent.Parent.Parent.FullName;

                return path;
            }
            #endregion
        }
        #endregion

        #region Constructors
        static Facade()
        {
            Helper helper = new Helper();

            Container = helper.GetContainer();
        }
        #endregion

        #region Fields
        private static readonly Container Container;
        #endregion

        #region Methods
        public static T Get<T>()
        {
            return Container.Resolve<T>();
        }
        #endregion
    }
}
