﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fasades.Common;

using Models;


namespace Fasades.Overview  
{
    public static partial class WeatherOverview
    {
        public abstract class Retrieving 
        {
            #region Static
            public static Retrieving New()
            {
                return Facade.Get<Retrieving>();
            }
            #endregion

            #region Properties
            public abstract Guid TownId { get; set; }
            public abstract DateTime Date { get; set; }
            #endregion

            #region Methods
            public abstract Task<OverviewModel> Go();
            #endregion
        }
    }
}
