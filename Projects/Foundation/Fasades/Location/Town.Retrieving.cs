﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fasades.Common;

using Models;


namespace Fasades.Location
{
    public static partial class Town
    {
        public abstract class Retrieving 
        {
            #region Static
            public static Retrieving New()
            {
                return Facade.Get<Retrieving>();
            }
            #endregion

            #region Methods
            public abstract Task<IReadOnlyList<TownModel>> Go();
            #endregion
        }
    }
}
